//
//  SecondViewController.swift
//  Maps
//
//  Created by Students on 03.04.18.
//  Copyright © 2018. g. Students. All rights reserved.
//

import UIKit
import MapKit

class SecondViewController: UIViewController,UITextFieldDelegate
{
    var delegate: SecondViewControllerDelegate?

    @IBOutlet weak var mapPointsFilterSegmentControl: UISegmentedControl!
    @IBOutlet weak var textboxNoticeLabel: UILabel!
    
    @IBOutlet weak var latitudeTexbox: UITextField!
    @IBOutlet weak var longitudeTextbox: UITextField!
    let defaults = UserDefaults.standard
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        mapPointsFilterSegmentControl.selectedSegmentIndex = defaults.integer(forKey: "mapPointsFilterIndex")
        latitudeTexbox.text = defaults.string(forKey: "latitude")
        longitudeTextbox.text = defaults.string(forKey: "longitude")
        
        self.latitudeTexbox.delegate = self
        self.longitudeTextbox.delegate = self
        
        if mapPointsFilterSegmentControl.selectedSegmentIndex == 1
        {
            _ = self.validateTextfields()
        }
    }
    
    @IBAction func toggleMapPointsToShow(_ sender: UISegmentedControl)
    {
        let index = sender.selectedSegmentIndex
        defaults.set(index, forKey: "mapPointsFilterIndex")
        
        switch index
        {
            case 0:
                delegate?.showOnlyWithDescriptionMapPoints()
                self.textboxNoticeLabel.isHidden = true
            case 1:
                delegate?.showALL()
                self.textboxNoticeLabel.isHidden = true
            
            case 2:
                 self.showOnlyNearbyMapPoints()
            
            default: break
        }
    }
    
    @IBAction func coordinatesTextFieldUpdate()
    {
        defaults.set(latitudeTexbox.text!, forKey: "latitude")
        defaults.set(longitudeTextbox.text!, forKey: "longitude")
        
        if mapPointsFilterSegmentControl.selectedSegmentIndex == 1
        {
            self.showOnlyNearbyMapPoints()
        }
    }
    // parada tuvakos
    func showOnlyNearbyMapPoints()
    {
        if self.validateTextfields()
        {
            delegate?.showOnlyNearbyMapPoints(fromLatitude: Double(latitudeTexbox.text!)!,fromLongitude:Double(longitudeTextbox.text!)!)
        }
    }
    // parbaudam vai kordinates ir pareizi ievaditas
    func validateTextfields()->Bool
    {
        if  let latidude     = Double(latitudeTexbox.text!),
            let longitude    = Double(longitudeTextbox.text!)
        {
            let loc = CLLocationCoordinate2D(latitude: latidude,longitude:longitude)
            
            if CLLocationCoordinate2DIsValid(loc)
            {
                self.textboxNoticeLabel.isHidden = true
                
                return true
            }
            else
            {
                self.textboxNoticeLabel.text = "Invalid coordinates"
                self.textboxNoticeLabel.isHidden = false
                return false
            }
        }
        else
        {
            self.textboxNoticeLabel.text = "Enter variables"
            self.textboxNoticeLabel.isHidden = false
            return false
        }
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }

}

protocol SecondViewControllerDelegate: class
{
    func showOnlyNearbyMapPoints(fromLatitude:Double, fromLongitude:Double)
    func showALL()
    func showOnlyWithDescriptionMapPoints()
}
