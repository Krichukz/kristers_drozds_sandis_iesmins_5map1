//
//  ViewController.swift
//  Maps
//
//  Created by Students on 03.04.18.
//  Copyright © 2018. g. Students. All rights reserved.
//

import UIKit
import MapKit

class ViewController: UIViewController, SecondViewControllerDelegate, CLLocationManagerDelegate, MKMapViewDelegate
{
    let defaults = UserDefaults.standard
    var myLocation: CLLocationCoordinate2D = CLLocationCoordinate2D()
    let locationManager = CLLocationManager()
    @IBOutlet weak var mapView: MKMapView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        
        self.myLocation = mapView.centerCoordinate
        
        switch defaults.integer(forKey: "mapPointsFilterIndex")
        {
            case 0:
                 showOnlyWithDescriptionMapPoints()
            case 1:
              
            showALL()
            case 2:
                showOnlyWithDescriptionMapPoints()
                if defaults.string(forKey: "latitude") != "", defaults.string(forKey: "longitude") != ""
                {
                    showOnlyNearbyMapPoints(fromLatitude: Double(defaults.string(forKey: "latitude")!)!, fromLongitude: Double(defaults.string(forKey: "longitude")!)!)
                }
                else
                {
                    showALL()
            }
            default: break
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if let filtersView = segue.destination as? SecondViewController
        {
            filtersView.delegate = self
        }
        
        
    }
    
    func addMapPoints(points: Array<NSDictionary>)
    {
        for point in points
        {
            let title       = point["Title"] as! String
            let latitude    = point["Latitude"] as! String
            let longitude   = point["Longitude"] as! String
            let description   = point["Description"] as! String
            
            if !(title.isEmpty),!(latitude.isEmpty),!(longitude.isEmpty)
            {
                self.addMapPoint(title: title, latitude: Double(latitude)!, longitude: Double(longitude)!, description: description)
            }
        }
    }
    
    func addMapPoint(title:String, latitude:Double, longitude:Double, description:String?)
    {
        let mapPoint = MKPointAnnotation();
        
        mapPoint.coordinate = CLLocationCoordinate2D(latitude: latitude,longitude:longitude)
        mapPoint.title      = title
        
        if description != nil
        {
            mapPoint.subtitle = description
        }
        
        mapView.addAnnotation(mapPoint)
    }
    
    func getMapPoints()->NSArray
    {
        var mapPoints: NSArray?
        
        if let path = Bundle.main.path(forResource: "MapPoints", ofType: "plist")
        {
            mapPoints = NSArray(contentsOfFile: path)
        }
        
        return mapPoints!
    }
    
    func kmfromPlace(from: CLLocationCoordinate2D, to:CLLocationCoordinate2D)->Double
    {
        let locA = CLLocation(latitude: from.latitude,longitude: from.longitude)
        let locB = CLLocation(latitude: to.latitude,longitude: to.longitude)
        
        let distance = locA.distance(from: locB) / 1000
    
        return distance
    }
    
    func showALL()
    {
        mapView.removeAnnotations(mapView.annotations)
        let mapPoints = self.getMapPoints()
        self.addMapPoints(points: mapPoints as! Array<NSDictionary>)
    }
    
    func showOnlyNearbyMapPoints(fromLatitude: Double, fromLongitude: Double)
    {
        self.showALL()
        
        let from = CLLocationCoordinate2D(latitude: fromLatitude,longitude:fromLongitude)
        self.addMapPoint(title: "Selected location", latitude: fromLatitude, longitude: fromLongitude, description: nil)
        
        for annotation in mapView.annotations
        {
            let distance = self.kmfromPlace(from: from, to: annotation.coordinate)
            
            if distance > 10
            {
                mapView.removeAnnotation(annotation)
            }
        }
    }
    
    func showOnlyWithDescriptionMapPoints()
    {
        mapView.removeAnnotations(mapView.annotations)
        let mapPoints = self.getMapPoints() as! Array<NSDictionary>
        
        for point in mapPoints
        {
            let title       = point["Title"] as! String
            let latitude    = point["Latitude"] as! String
            let longitude   = point["Longitude"] as! String
            let description   = point["Description"] as! String
            
            if !(title.isEmpty),!(latitude.isEmpty),!(longitude.isEmpty), !(description.isEmpty)
            {
                self.addMapPoint(title: title, latitude: Double(latitude)!, longitude: Double(longitude)!, description: description)
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        self.myLocation = locations[0].coordinate
        let region = MKCoordinateRegion(center: locations[0].coordinate, span: mapView.region.span)
        
        mapView.setRegion(region, animated: true)
    }
    

    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView)
    {
        let request             = MKDirectionsRequest()
        request.source          = MKMapItem(placemark: MKPlacemark(coordinate: self.myLocation, addressDictionary: nil))
        request.destination     = MKMapItem(placemark: MKPlacemark(coordinate:(view.annotation?.coordinate)!, addressDictionary: nil))

    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer
    {
        let renderer = MKPolylineRenderer(overlay:overlay)
       
        
        return renderer
    }
}

